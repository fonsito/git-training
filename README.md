## Synopsis

The content of this repo involves slides, docs and some examples about git workflow and Product Life Cycle

## Motivation

The main goal is to improve the knowledge of git and be awareness of the importance of a good management and development of the Product Life Cycle, having git in mind as a multipurpose tool for it.

## Contributors

Any contribution is welcome by anyone who want to improve, fix or correct any typo or info.
Feel free to clone this repo and edit whatever you want.

## Index

Draft index:

    PLC (Product Life Cycle)
        What is PLC?
        Why is PLC so important at software building?
        From RUP to Agile

    Agile Best Practices
        It's all about workflows
        An approach to "Workflow for Agile Flow"
        Lets decide our successful workflow

    GIT
        From SVN to Git
        Distributed repositories?
        What is NOT Git?
        What is really Git?
        Traveling across dimensions
            The roadmaps to success
            master, develop, features, hotfix, release...
            When to create a branch? Be smart!
        Practical cases
        Traveling in time (For a Second workshop)
            Cherry picks
            Merges and mix it up
            Rebases and resets
            All you can imagine can be done
        Introducing Perforce
    What have we learnt?
    Question Time

## License

<center><a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a></center><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">"git... what?"</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="about.me/fonsito" property="cc:attributionName" rel="cc:attributionURL">Alfonso Maestre</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Reconocimiento-NoComercial 4.0 Internacional License</a>.<br />Creado a partir de la obra en <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/fonsito/git-training" rel="dct:source">https://gitlab.com/fonsito/git-training</a>.